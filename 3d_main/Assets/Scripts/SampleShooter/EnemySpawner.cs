using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemySpawner : MonoBehaviour
{
   [SerializeField]
   private List<Transform> spawnPoints;

   [SerializeField]
   private float spawnTime = 0.5f;

   [SerializeField]
   private EnemyPool enemyPool;

   [SerializeField]
   private FasterEnemyPool fasterEnemyPool;

   public IEnumerator Start()
   {
      enemyPool.Initialize(20);
      fasterEnemyPool.Initialize(20);
      var counter = 0;
      var isEven = false;
      while (true)
      {
         var randomIndex = Random.Range(0, spawnPoints.Count);
         if (isEven)
         {
            var enemy = fasterEnemyPool.GetFromPool(spawnPoints[randomIndex].position, 
               spawnPoints[randomIndex].rotation);
            enemy.AddListener(fasterEnemyPool.ReturnToPool);
            enemy.StartMovement();
         }
         else
         {
            var enemy = enemyPool.GetFromPool(spawnPoints[randomIndex].position, 
               spawnPoints[randomIndex].rotation);
            enemy.AddListener(enemyPool.ReturnToPool);
            enemy.StartMovement();
         }

         isEven = !isEven;
         yield return new WaitForSeconds(spawnTime);
      }
   }
}
