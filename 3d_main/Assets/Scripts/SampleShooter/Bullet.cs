using System;
using UnityEngine;

public class Bullet : MonoBehaviour, IPoolable
{
    [SerializeField]
    private Rigidbody rigidbody;

    [SerializeField]
    private float speed;

    private Action<Bullet> onDestroyCallback;

    public void StartMovement()
    {
        rigidbody.AddForce(Vector3.forward * speed, ForceMode.Impulse);
    }

    public void AddListener(Action<Bullet> callback)
    {
        onDestroyCallback += callback;
    }

    private void OnCollisionEnter(Collision collision)
    {
        onDestroyCallback?.Invoke(this);
    }

    public void PrepareForActivate()
    {
        onDestroyCallback = null;
    }

    public void PrepareForDeactivate()
    {
        rigidbody.velocity = Vector3.zero;
    }
}
