using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// interface: IPlayer
// parameter: TParameter

public class Pool<TItem> : MonoBehaviour
    where TItem : MonoBehaviour, IPoolable
{
    private int size; //Ile maksymalnie przedmiotów pool może przechować
    public int Count => internalCollection.Count; //Ile przedmiotów aktualnie znajduje się w poolu

    [SerializeField]
    private TItem originalPrefab; //przepis na obiekt -> prefab z którego generujemy
    private Stack<TItem> internalCollection; //stos wygenerowanych obiektów

    public void Initialize(int size)
    {
        internalCollection = new Stack<TItem>(size);
        for (var i = 0; i < size; ++i)
        {
            var newObj = SpawnNewObject();
            internalCollection.Push(newObj); //Push -> dodaje do kolekcji; Pop -> zwraca pierwszy obiekt z góry i usuwa z kolekcji
        }

        this.size = size;
    }

    public TItem GetFromPool(Vector3 position)
    {
        TItem toReturn = 
            internalCollection.Count == 0 ? 
            SpawnNewObject() : 
            internalCollection.Pop();
        
        toReturn.transform.position = position;
        toReturn.transform.rotation = originalPrefab.transform.rotation;
        
        toReturn.transform.SetParent(null);
        toReturn.PrepareForActivate();
        toReturn.gameObject.SetActive(true);
        
        return toReturn;
    }
    
    public TItem GetFromPool(Vector3 position, Quaternion rotation)
    {
        TItem toReturn = GetFromPool(position);
        toReturn.transform.rotation = rotation;
        return toReturn;
    }

    public void ReturnToPool(TItem item)
    {
        if (internalCollection.Count < size)
        {
            item.PrepareForDeactivate();
            item.gameObject.SetActive(false);
            item.transform.SetParent(this.transform);
            internalCollection.Push(item);
        }
        else
        {
            Destroy(item.gameObject);
        }
    }

    private TItem SpawnNewObject()
    {
        var newObj = Instantiate(originalPrefab, Vector3.zero,
            originalPrefab.transform.rotation, this.transform);
        newObj.gameObject.SetActive(false);
        return newObj;
    }
}
