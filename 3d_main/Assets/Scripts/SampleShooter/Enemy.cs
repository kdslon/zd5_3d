using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Enemy : MonoBehaviour, IPoolable
{
    [SerializeField]
    private Rigidbody rb;

    [SerializeField]
    private float speed;

    private Action<Enemy> onDestroyCallback; //specjalna zmienna, która przechowuje metody o sygnaturze "void Nazwa(Enemy enemy)"

    public void StartMovement()
    {
        rb.AddForce(transform.forward * speed, ForceMode.Impulse);
    }

    public void AddListener(Action<Enemy> callback)
    {
        onDestroyCallback += callback;
    }

    public void PrepareForActivate()
    {
        onDestroyCallback = null;
    }

    public void PrepareForDeactivate()
    {
        rb.velocity = Vector3.zero;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bullet") || collision.gameObject.CompareTag("Wall"))
        {
            //  taki zapis: obiekt?.Metoda() -> sprawdza czy obiekt jest różny od null
            onDestroyCallback?.Invoke(this);
        }
    }
}
