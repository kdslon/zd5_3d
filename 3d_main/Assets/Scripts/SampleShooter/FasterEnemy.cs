using System;
using UnityEngine;

public class FasterEnemy : MonoBehaviour, IPoolable
{
    [SerializeField]
    private Rigidbody rb;

    [SerializeField]
    private float speed;
    
    private Action<FasterEnemy> onDestroyCallback;
    
    public void StartMovement()
    {
        rb.AddForce(transform.forward * speed, ForceMode.Impulse);
    }

    public void AddListener(Action<FasterEnemy> callback)
    {
        onDestroyCallback += callback;
    }

    public void PrepareForActivate()
    {
        onDestroyCallback = null;
    }

    public void PrepareForDeactivate()
    {
        rb.velocity = Vector3.zero;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bullet") || collision.gameObject.CompareTag("Wall"))
        {
            onDestroyCallback?.Invoke(this);
        }
    }
}
