using UnityEngine;
using UnityEngine.InputSystem;

public class InputController : MonoBehaviour
{
    public float MovementValue { get; private set; }
    private bool fireValue;
    public bool FireValue {
        get
        {
            var val = fireValue;
            fireValue = false;
            return val;
        }
    }

    public void OnMovement(InputAction.CallbackContext ctx)
    {
        MovementValue = ctx.ReadValue<float>();
    }

    public void OnFire(InputAction.CallbackContext ctx)
    {
        if(!fireValue)
            fireValue = ctx.action.WasPerformedThisFrame();
    }
}
