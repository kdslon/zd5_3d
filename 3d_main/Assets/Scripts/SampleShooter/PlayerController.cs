using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private float speed;

    [SerializeField]
    private Vector2 limits;

    [SerializeField]
    private Transform spawnPos;

    [SerializeField]
    private BulletPool bulletPool;

    [SerializeField]
    private InputController inputController;

    private void Start()
    {
        bulletPool.Initialize(50);
        transform.position = new Vector3(0f, transform.position.y, transform.position.z);
    }

    void Update()
    {
        var possibleMove = Vector3.zero;
        possibleMove = Vector3.right * (inputController.MovementValue * speed * Time.deltaTime);

        if(inputController.FireValue)
        {
            var newBullet = bulletPool.GetFromPool(spawnPos.position, Quaternion.identity);
            newBullet.AddListener(bulletPool.ReturnToPool);
            newBullet.StartMovement();
        }

        var possiblePos = transform.position + possibleMove;
        possiblePos.x = Mathf.Clamp(possiblePos.x, limits.x, limits.y);
        transform.position = possiblePos;
    }
}
