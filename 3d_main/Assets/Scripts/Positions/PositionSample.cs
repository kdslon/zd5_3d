using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class PositionSample : MonoBehaviour
{
    public void Test()
    {
        var globalPos = transform.position;
        var localPos = transform.localPosition;

        Vector3 a = new Vector3();
        Vector3 b = new Vector3();

        var dir = (b - a).normalized;

        Quaternion rot = transform.rotation;
        Quaternion neutralQuat = Quaternion.identity; //Quaternion.identity => (0,0,0)
        Vector3 angles = rot.eulerAngles; //Przeliczamy Quaternion na Vector3 (0,0,0,1) => (0,0,0)
        Quaternion anglesQuat = Quaternion.Euler(angles); //Przeliczamy Vector3 na Quaterniona => (0,0,0) => (0,0,0,1)
        
        //Vector3.Dot()
    }
    

    private void OnDrawGizmos()
    {
        #if UNITY_EDITOR
        Handles.Label(Vector3.zero, "JAKIŚ TEXT");
        #endif
    }
}
