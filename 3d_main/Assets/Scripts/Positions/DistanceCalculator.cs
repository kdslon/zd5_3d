using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEditor;
using UnityEngine;

public class DistanceCalculator : MonoBehaviour
{
    [SerializeField]
    private Transform pointA;

    [SerializeField]
    private Transform pointB;

    [SerializeField]
    private Transform pointC;

    [SerializeField]
    private bool isGlobal = true;

    private void OnDrawGizmos()
    {
        CalculateDistance();
        CalculateDirection();
    }

    private void CalculateDistance()
    {
        Gizmos.color = Color.red;
        if (isGlobal)
        {
            Gizmos.DrawLine(pointA.position, pointB.position);
            Handles.Label(Vector3.Lerp(pointA.position, pointB.position, 0.5f),
                Vector3.Distance(pointA.position, pointB.position)
                    .ToString(CultureInfo.InvariantCulture));
        }
        else
        {
            Gizmos.DrawLine(pointA.localPosition, pointB.localPosition);
            Handles.Label(Vector3.Lerp(pointA.localPosition, pointB.localPosition, 0.5f),
                Vector3.Distance(pointA.localPosition, pointB.localPosition)
                    .ToString(CultureInfo.InvariantCulture));
        }

        var globalPos = transform.TransformPoint(pointA.localPosition);
        Gizmos.DrawLine(globalPos, pointC.position);
        Handles.Label(Vector3.Lerp(globalPos, pointC.position, 0.5f),
            Vector3.Distance(globalPos, pointC.position)
                .ToString(CultureInfo.InvariantCulture));        
    }

    private void CalculateDirection()
    {
        Gizmos.color = Color.blue;
        if (isGlobal)
        {
            var dir = (pointB.position - pointA.position).normalized;
            Gizmos.DrawRay(pointA.position, dir);
        }
        else
        {
            var dir = (pointB.localPosition - pointA.localPosition).normalized;
            Gizmos.DrawRay(pointA.localPosition, dir);
        }
    }

}