using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphericalCalculation : MonoBehaviour
{
    public static Vector3 CalculatePointOnCircleXY(float radius, float angleInDeg)
    {
        float x = radius * Mathf.Cos(angleInDeg * Mathf.Deg2Rad);
        float y = radius * Mathf.Sin(angleInDeg * Mathf.Deg2Rad);
        return new Vector3(x, y, 0f);
    }

    public static Vector3 CalculatePointOnSphere(float rotationX, float rotationY, float radius)
    {
        float x = radius * Mathf.Cos(rotationX * Mathf.Deg2Rad) * Mathf.Sin(rotationY * Mathf.Deg2Rad);
        float y = radius * Mathf.Sin(rotationX * Mathf.Deg2Rad) * Mathf.Sin(rotationY * Mathf.Deg2Rad);
        float z = radius * Mathf.Cos(rotationY * Mathf.Deg2Rad);
        return new Vector3(x, y, z);
    }
}
