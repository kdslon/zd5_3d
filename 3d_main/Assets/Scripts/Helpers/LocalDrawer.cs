using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class LocalDrawer : MonoBehaviour
{
    public int xSize;
    public int zSize;
    public int ySize;

    public Color xAxisColor;
    public Color yAxisColor;
    public Color zAxisColor;

    public bool displayAlways;

    private void OnDrawGizmos()
    {
        if (!displayAlways)
            return;

        Gizmos.matrix = transform.localToWorldMatrix;
        Handles.matrix = transform.localToWorldMatrix;

        Gizmos.color = xAxisColor;
        for (int i = -xSize; i <= xSize; i++)
        {            
            Gizmos.DrawLine(new Vector3(i, 0f, zSize), new Vector3(i, 0f, -zSize));
            Handles.Label(new Vector3(i, 0f, 0f), $"{i}");
        }

        Gizmos.color = zAxisColor;
        for (int i = -zSize; i <= zSize; i++)
        {
            Gizmos.DrawLine(new Vector3(xSize, 0f, i), new Vector3(-xSize, 0f, i));
            Handles.Label(new Vector3(0f, 0f, i), $"{i}");
        }

        Gizmos.color = yAxisColor;
        Gizmos.DrawLine(new Vector3(0f, -ySize, 0f), new Vector3(0f, ySize, 0f));
        for(int i = -ySize; i <= ySize; i++)
        {
            Handles.Label(new Vector3(0f, i, 0f), $"{i}");
        }
    }
}
