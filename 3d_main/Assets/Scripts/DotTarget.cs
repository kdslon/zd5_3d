using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DotTarget : MonoBehaviour
{
    [SerializeField]
    private MeshRenderer meshRenderer;

    public void ChangeColor(Color newColor)
    {
        meshRenderer.material.color = newColor;
    }
}
