using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereColliderGizmos : MonoBehaviour
{
    [SerializeField]
    private SphereCollider sphereCollider;
    
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        var scale = Mathf.Max(
            Mathf.Abs(transform.localScale.x), 
            Mathf.Abs(transform.localScale.y), 
            Mathf.Abs(transform.localScale.z));

        Gizmos.DrawWireSphere(transform.position + sphereCollider.center, sphereCollider.radius * scale);
    }
}
