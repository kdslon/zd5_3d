using UnityEngine;

public class GizmosExample : MonoBehaviour
{
    [Header("Box")]
    public Vector3 boxSize;
    public Vector3 boxPosition;
    public Color boxColor;

    [Header("Sphere")]
    public float radius;
    public Vector3 spherePosition;
    public Color sphereColor;

    [Header("Mesh")]
    public Mesh mesh;
    public Vector3 meshPosition;
    public Vector3 meshRotation;
    public Color meshColor;

    [Header("Line")]
    public Vector3 lineStart;
    public Vector3 lineFinish;
    public Color lineColor;

    #region Box
    public void DrawWireBox()
    {
        Gizmos.color = boxColor;
        Gizmos.DrawWireCube(boxPosition, boxSize);
    }

    public void DrawBox()
    {
        Gizmos.color = boxColor;
        Gizmos.DrawCube(boxPosition, boxSize);
    }
    #endregion

    #region Sphere
    public void DrawWireSphere()
    {
        Gizmos.color = sphereColor;
        Gizmos.DrawWireSphere(spherePosition, radius);
    }

    public void DrawSphere()
    {
        Gizmos.color = sphereColor;
        Gizmos.DrawSphere(spherePosition, radius);
    }
    #endregion

    #region Mesh
    public void DrawWireMesh()
    {
        Gizmos.color = meshColor;
        Gizmos.DrawWireMesh(mesh, meshPosition, Quaternion.Euler(meshRotation));
    }

    public void DrawMesh()
    {
        Gizmos.color = meshColor;
        Gizmos.DrawMesh(mesh, meshPosition, Quaternion.Euler(meshRotation));
    }
    #endregion

    public void DrawWireLine()
    {
        Gizmos.color = lineColor;
        Gizmos.DrawLine(lineStart, lineFinish);
    }

    private void OnDrawGizmos()
    {
        DrawWireBox();
        DrawWireSphere();
        DrawWireMesh();
        DrawWireLine();
    }

    private void OnDrawGizmosSelected()
    {
        DrawBox();
        DrawSphere();
        DrawMesh();
    }
}