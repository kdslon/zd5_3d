using System;
using System.Collections.Generic;
using UnityEngine;

public class GizmosManager : MonoBehaviour
{
    [SerializeField]
    private List<Transform> objectPoses;

    private void DrawGizmosForList()
    {
        Gizmos.color = Color.red;
        for (int i = 0; i < objectPoses.Count; ++i)
        {
            Gizmos.DrawWireCube(objectPoses[i].position, objectPoses[i].localScale);
        }
    }

    private void DrawLineBetweenObjects()
    {
        Gizmos.color = Color.white;
        for (int i = 0; i < objectPoses.Count; ++i)
        {
            //0 - 1
            //1 - 2
            //2 - 3
            //3 - 4[OUT OF RANGE]
            Gizmos.DrawLine(objectPoses[i].position, objectPoses[(i+1) % objectPoses.Count].position);
        }
    }

    private void OnDrawGizmos()
    {
        DrawGizmosForList();
        DrawLineBetweenObjects();
    }
}
