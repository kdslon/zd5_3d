using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshGizmosManager : MonoBehaviour
{
    [SerializeField]
    private List<Mesh> meshes;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        var currentPosition = this.transform.position;
        var eulerRot = this.transform.rotation.eulerAngles;
        foreach (var mesh in meshes)
        {
            Gizmos.DrawWireMesh(mesh, currentPosition, Quaternion.Euler(eulerRot));
            currentPosition.z += 1;
            eulerRot.y += 5;
        }
    }
}
