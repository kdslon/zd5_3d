using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEditor;
using UnityEngine;

public class DotExample : MonoBehaviour
{
    [SerializeField]
    private List<DotTarget> targets;

    [SerializeField]
    private Color startColor;

    [SerializeField]
    private Color endColor;

    private float CalculateDot(Transform target)
    {
        var dir = (target.position - transform.position).normalized;
        return Vector3.Dot(transform.forward, dir);
    }

    private void DisplayGizmo(Transform target)
    {
        var roundedDot = (float)Math.Round(CalculateDot(target), 2);
        Gizmos.color = Color.Lerp(startColor, endColor, roundedDot);
        Gizmos.DrawLine(transform.position, target.position);
        Handles.Label(
            Vector3.Lerp(this.transform.position, target.transform.position, 0.5f), 
            roundedDot.ToString(CultureInfo.InvariantCulture));
    }

    private void Update()
    {
        foreach (var target in targets)
        {
            var roundedDot = (float)Math.Round(CalculateDot(target.transform), 2);
            if(roundedDot >= 0.9f && roundedDot <= 1f)
                target.ChangeColor(Color.yellow);
            else
                target.ChangeColor(Color.white);
        }
    }

    private void OnDrawGizmos()
    {
        foreach (var target in targets)
        {
            DisplayGizmo(target.transform);
        }
    }
}
